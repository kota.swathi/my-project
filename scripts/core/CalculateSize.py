import math


class CalculateSize:
    @staticmethod
    def circle(radius):
        area = 3.14 * radius * radius
        perimeter = 2 * 3.14 * radius
        res = (area, perimeter)
        return res

    @staticmethod
    def square(side):
        area = side * side
        perimeter = 4 * side
        res = (area, perimeter)
        return res
